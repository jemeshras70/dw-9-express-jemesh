// Make express Application
// Assign port to that application
import express, { json } from "express";

import firstRouter from "./src/Router/firstRouter.js";
import bikeRouter from "./src/Router/bikeRouter.js";
import genderRouter from "./src/Router/genderRouter.js";
import traineeRouter from "./src/Router/traineeRouter.js";

let expressApp = express();
expressApp.use(json());
let port = 8000;
expressApp.listen(port, () => {
  console.log(`Express app is listening at port ${port}`);
});

/* 
expressApp.use(
  (req, res, next) => {
    console.log("I am application middleware 1");
    next();
  },
  (req, res, next) => {
    console.log("I am application middleware 2");
    next();
  },
  (req, res, next) => {
    console.log("I am application middleware 3");
    next();
  }
); 
*/

expressApp.use("/", firstRouter);

expressApp.use("/bike", bikeRouter);
