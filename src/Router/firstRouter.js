import { Router } from "express";

let firstRouter = Router();

firstRouter
  .route("/")
  .post((req, res) => {
    console.log("body send data: ", req.body);
    // console.log("query send data: ", req.query);

    res.json("home post");
    // res.json("Jemesh") // Only one response for one request is possible
  })
  .get(() => {
    console.log("Home get");
  })
  .delete(() => {
    console.log("home delete");
  });

firstRouter
  .route("/name")
  .get(() => {
    console.log("get name");
  })
  .patch(() => {
    console.log("patch name");
  })
  .delete(() => {
    console.log("delete name");
  });

export default firstRouter;
