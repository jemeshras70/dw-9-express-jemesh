import { Router } from "express";

let genderRouter = Router();

genderRouter
  .route("/gender")
  .post(() => {
    console.log("Post Gender");
  })
  .get(() => {
    console.log("Get Gender");
  })
  .patch(() => {
    console.log("Patch Gender");
  })
  .delete(() => {
    console.log("Delete Gender");
  });

export default genderRouter;
