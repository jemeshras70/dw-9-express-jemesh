import { Router } from "express";

let bikeRouter = Router();

bikeRouter
  .route("/")
  .post(
    (req, res, next) => {
      console.log("I am middleware 1");
      next("a"); //To trigger next normal middleware we have to call next() & to call error middleware we have to call next with some parameter.
    },
    (err, req, res, next) => {
      console.log("I am middleware 2");
      let er1 = new Error();
      next(err);
    },
    (err, req, res, next) => {
      console.log("I am middleware 3");
      next();
    },
    (req, res, next) => {
      console.log("I am middleware 4");
    }
  )
  .get(() => {
    console.log("get bike");
  })
  .patch(() => {
    console.log("patch bike");
  })
  .delete(() => {
    console.log("delete bike");
  });

export default bikeRouter;
