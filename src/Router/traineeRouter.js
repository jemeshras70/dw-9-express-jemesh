import { Router } from "express";
let traineeRouter = Router();
traineeRouter
  .route("/trainee")
  .post((req, res) => {
    res.json({ success: true, message: "school" });
  })
  .get((req, res) => {
    res.json({ success: true, message: "school read successfully" });
  })
  .patch((req, res) => {
    res.json({ success: true, message: "school updated successfully" });
  })
  .delete((req, res) => {
    res.json({ success: true, message: "school deleted successfully" });
  });
export default traineeRouter;
